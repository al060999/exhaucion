package exhaucion;

import javax.swing.JOptionPane;

public class Exhaucion {

    public static void main(String[] args) {
        
        int Nlados = 6;
        double Ladoinicial = 1;
        double x = 0;
        double a = 0;
        double b = 0;
        double Nuevolado = 0;
        double P = 0;
        double PD = 0;
        
        for(int i = 1; i < 13; i++){
            x = (Ladoinicial/2);
            a = Math.sqrt(1-(Math.pow(x,2)));
            b = (1-a);
            Nuevolado = Math.sqrt(Math.pow(x,2) + Math.pow(b,2));
            P = Nlados*Ladoinicial;
            PD = P/2;
                
            System.out.print("N lados = " + Nlados);
            System.out.print("  Lado inical = " + Ladoinicial);
            System.out.print("  x = " + x);
            System.out.print("  a = " + a);
            System.out.print("  b = " + b);
            System.out.print("  Nuevo lado = " + Nuevolado);
            System.out.print("  P = " + P);
            System.out.print("  P/D = " + PD);
            System.out.println("");
                
            Nlados = (Nlados*2);
            Ladoinicial = Nuevolado;
        }
    }   
}
